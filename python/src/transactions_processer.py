from services import db_service

from decimal import Decimal


def handler(event, context):
    print(event)
    transaction = event["Records"][0]["dynamodb"]["NewImage"]
    print(transaction)
    sender_id = transaction["SenderAccountId"]["S"]
    receiver_id = transaction["ReceiverAccountId"]["S"]
    transaction_amount = Decimal(transaction["Balance"]["N"])
    print(f"{sender_id}\n{receiver_id}")
    sender_account, receiver_account = db_service.get_accounts_from_transaction(sender_id, receiver_id)
    transfer_credit(sender_account, receiver_account, transaction_amount)
    print(f"{sender_account}\n{receiver_account}")
    db_service.update_accounts_from_transaction(sender_account, receiver_account)


def transfer_credit(sender: dict, receiver: dict, amount):
    if sender is not None:
        sender["Balance"] -= amount
    receiver["Balance"] += amount
