from utilities import configuration_utility

import boto3
from boto3.dynamodb.types import TypeDeserializer, TypeSerializer


def _get_client(session=None, region="eu-west-1"):
    if session is None:
        session = boto3.Session()
    return session.client("dynamodb", region_name=region)


def get_accounts_from_transaction(sender_id: str, receiver_id: str):
    sender_account = _gsi_query_dynamodb_items(configuration_utility.get_account_table_name(), "AccountId", sender_id, "AccountId-Index")[0] if sender_id != "init" else None
    receiver_account = _gsi_query_dynamodb_items(configuration_utility.get_account_table_name(), "AccountId", receiver_id, "AccountId-Index")[0]
    print(f"{sender_account=}\n{receiver_account=}")
    return _remove_dynamodb_typing(sender_account), _remove_dynamodb_typing(receiver_account)


def update_accounts_from_transaction(sender_account: dict, receiver_account: dict):
    if sender_account is not None:
        _put_dynamodb_item(configuration_utility.get_account_table_name(), sender_account)
    _put_dynamodb_item(configuration_utility.get_account_table_name(), receiver_account)


def _put_dynamodb_item(table_name: str, item: dict) -> dict:
    client = _get_client()
    dynamodb_item = _add_dynamodb_typing(item)
    response = client.put_item(
        TableName=table_name,
        Item=dynamodb_item
    )
    print(f"response from _put_dynamodb_item: {response}")
    return response


def _gsi_query_dynamodb_items(table_name: str, key: str, val: str, index_name: str) -> dict:
    print(f"_gsi_query_dynamodb_item for {table_name} started!")
    paginator = _get_paginator("query")
    response_iterator = paginator.paginate(
        IndexName=index_name,
        TableName=table_name,
        Select="ALL_ATTRIBUTES",
        KeyConditionExpression=f"{key} = :val",
        ExpressionAttributeValues={":val": {"S": val}}
    )
    items = []
    for item in response_iterator:
        items.extend(item["Items"])
    print(f"{items=}")
    return items


def _get_paginator(operation: str, client=None):
    if client is None:
        client = _get_client()
    #  Dont catch exception from get_paginator for invalid operations since it will crash anyway. boto3 errors are clear as it is.
    paginator = client.get_paginator(operation)
    return paginator


def _remove_dynamodb_typing(dynamodb_data: dict) -> dict:
    print(f"_remove_dynamodb_typing deserializing {dynamodb_data}")
    if dynamodb_data is None:
        return None
    deserializer = TypeDeserializer()
    deserialized_data = {k: deserializer.deserialize(v) for k, v in dynamodb_data.items()}
    print(f"Deserialized data: {dynamodb_data}")
    return deserialized_data


def _add_dynamodb_typing(dict: dict) -> dict:
    serializer = TypeSerializer()
    serialized_data = {k: serializer.serialize(v) for k, v in dict.items()}
    return serialized_data
